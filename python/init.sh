#!/usr/bin/sh
if [[ -z "$1" ]]; then
   	printf "Create simple python file\n"
	printf "Usage: $0 name\n"
	exit 1
fi

cp $STARTER/python/python.py $1.py
echo "The script $1 was correctly generated\n"
