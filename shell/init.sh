#!/usr/bin/sh
if [[ -z "$1" ]]; then
   	printf "Create a bash script\n"
	printf "Usage: $0 filename\n"
	exit 1
fi

cp $STARTER/shell/shell.sh $1.sh
echo "script $1 was correctly generated"
