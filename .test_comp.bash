#/usr/bin/env bash

comp(){
	languages=()
	input="$STARTER/languages"

	while IFS= read -r var
		do
			language+= "${var//[$'\n']}"
	done < "$input"
	echo ${languages[*]}
	
	# current word being autocompleted
	local cur=${COMP_WORDS[COMP_CWORD]}
	
	COMPREPY=(`compgen -W "${languages[*]}" -- $cur`)
}

comp() 

complete -F comp new
