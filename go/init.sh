#!/usr/bin/sh

if [[ -z $GOPATH ]]; then
	echo "GOPATH doesn't exists"
	exit 1
fi

if [[ -z $GIT ]]; then
	echo "GIT doesn't exists"
	exit 1
fi

PROJECT_DIR=$GOPATH/src/gitlab.com/$GIT
if [[ -z "$1" ]]; then
   	printf "create a golang project in the right place\n"
	printf "Usage: $0 project_name\n"
	exit 1
fi

WD=$PROJECT_DIR/$1
mkdir -p $WD/
cp -r $STARTER/go/sample $WD/.
cd $WD
git init > /dev/null
touch .gitignore
git add .
git commit -m "init commit"
printf "go project $1 was correctly initialized\n"
printf "path is "
realpath $WD
