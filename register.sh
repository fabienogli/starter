#!/usr/bin/sh
if [[ -z "$1" ]]; then
   	printf "Register a language\n"
	printf "Usage: $0 new_language\n"
	exit 1
fi

language=$1

# check if it does already exists
if [ -d "$STARTER/$language" ]; then
	printf "$language starter already exists"
	exit 1
fi
mkdir $STARTER/$language
cp $STARTER/shell/init.sh $STARTER/$language/init.sh

echo "$language recorded\n"
