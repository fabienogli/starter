# Starter pack for application depending of language
```$STARTER``` should be defined in your environment 
You need to add the following to your bashrc:  
```source $STARTER/.comp.bash```
  
## Supported languages are:
  - python  
  - go  
  - shell  
